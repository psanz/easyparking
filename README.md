# EasyParking
** Demo project for a Web Parking toll manager built with Spring Boot + Java 11 + Mongo DB**

## Description

A single Application instance is able to manage multiple parkings via Rest controllers and a Mongo DB.

**The App has 3 controllers accessible by the parking owners:**

1. Parking controller: parking CRUD operations using a DTO to define its pricing policy and places by vehicle type. More info in Parking resource doc.
2. Access controller: manage vehicle arrival and departure of parking slots
3. Billing controller: to manage billing of clients

**DB resources are exposed through rest repositories, that might be used for admin. They are:**

1. Client: Includes client arrival, departure, bill, parking slot, ...
2. Parking: Contains Pricing Policy and slot inventory

**Persistence is achieved using a Mongo DB.** 

Since this is a demo, we're using an embedded DB, but it will need to be replaced by a DB server.

**Modules are structured vertically by classes (and not horizontally by layers). They are:**

1. access
2. billing
3. parking
4. client
5. shared (Global AOP, Exceptions, ...) 
6. config

**Using AOP to:**

1. Handle exceptions properly in Rest end-points.
2. Listen Mongo convertions to use sequences to generate Ids and to perform complex input validations.


**Security is not in scope of the demo.**

Rest end-points should be secured and the access restricted by Parking to authorized users.

### Maintenance and Scalability
Client and Parking information (including inventory) is stored in DB.

Therefore, the system can be rebooted and continue to operate normally.
Access and billing tasks are executed transactionally, meaning that any request finished abruptly will not cause inconsistencies in DB. 

As safeguard, Client and Parking resource Rest Repository controller can be used to manage client info and parking slot inventory.  

Client access, billing and Parking (Pricing Policy & Inventory) updates grant MUTEX using memory locks by Parking. 

Therefore, the architecture can be scaled: *N parkings -> 1 Web App -> 1 DB* (but the same parking cannot be present in two Web App / DB)

---

## Usage
Sources can be built with jdk 11 with commands: 

*mvn clean package && java -jar target\parking-0.0.1-SNAPSHOT.jar*

For the demo, the application has been configured to build on boot a new Parking with Sedan, 35kw and 50kw 
slots and a pricing policy with a fixed amount + each hour spent in the parking

### Parking controller
It uses a DTO to CRUD parkings. The DTO includes the pricing policy and the number of places per vehicle type.

The Parking resource, instead of the number of places, contains the slot inventory (which slots are free / occupied), in order to be able to send clients to a specific slot on arrival, or reject them if the parking is full.

The valid pricing policies are defined in the enum demo.easy.parking.billing.PricingPolicy (nowadays there are two: HOURS and FIXED_PLUS_HOURS)

Each policy is linked to a list of variables, defined in th the enum demo.easy.parking.billing.PricingPolicyVariable (nowadays there are two: HOUR_PRICE and FIXED).

Therefore, any parking stored must contain a value for each variable linked to the PricingPolicy.

For more information about the controller please refer to the integration tests
*demo.easy.parking.parking.ParkingControllerIntegrationTest*

#### Access controller
Consist mainly in two operations: letIn and letOut. 

LetIn petition might be rejected in the parking is full (for the vehicle type).

We don't want to keep hostages, so we always allow clients to leave a parking slot.

For more information about the controller please refer to the integration tests
*demo.easy.parking.access.AccessControllerIntegrationTest*

#### Billing controller
The service has the sole purpose to bill a client that has already left the parking, sending back the bill in ISO-3166.

The bill is stored in the Client resource.

For more information about the controller please refer to the integration tests
*demo.easy.parking.billing.BillingControllerIntegrationTest*


#### Rest Repositories
Resources (Client and Parking) are exposed for CRUD via Rest end-points using spring-data-rest-hal-browser.

Those end-points are meant to be restricted to system admins.

Client resource is meant to be used as a record, not only to drive the access and billing workflow.

**Hope you like it. Enjoy!**





