package demo.easy.parking.billing;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.joda.money.Money;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import demo.easy.parking.AbstractWebTest;
import demo.easy.parking.client.Client;
import demo.easy.parking.client.ClientRepository;
import demo.easy.parking.parking.Parking;
import demo.easy.parking.parking.ParkingRepository;
import demo.easy.parking.parking.ParkingRepositoryTest;

/**
 * BillingController Integration tests (chained WS calls down to embedded DB)
 */
public class BillingControllerIntegrationTest extends AbstractWebTest {

    @Autowired
    ParkingRepository parkingRepository;

    @Autowired
    ClientRepository clientRepository;

    Parking parking;
    String uri = "/bill/";

    @BeforeEach
    public void setUp() throws Exception {
        super.setUp();
        parking = ParkingRepositoryTest.buildParkingFixedPlusHours();
        parking = parkingRepository.save(parking);
    }

    @AfterEach
    public void tearDown() {
        parkingRepository.delete(parking);
    }

    /**
     * Call OK to bill client who has already left the parking.
     */
    @Test
    public void billOk() throws Exception {
        Client client = new Client();
        client.setPlate("ABC123");
        client.setParkingId(parking.getId());
        client.setSlotType(ParkingRepositoryTest.SEDAN);
        client.letIn(0);
        client.letOut();
        client.setDeparture(LocalDateTime.now().plusHours(10));
        client = clientRepository.save(client);
        Assertions.assertNotNull(client, "saved client null");
        Assertions.assertNotNull(client.getId(), "client Id null");

        // Create new parking
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri + client.getId())).andReturn();
        // check created parking
        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        Assertions.assertEquals(HttpStatus.OK.value(), status, "json response: " + content);
        Money money = super.mapFromJson(content, Money.class);

        Assertions.assertNotNull(money);
        Assertions.assertNotNull(money.getAmount());
        Assertions.assertEquals(0, BigDecimal.valueOf(5 + (2 * 10)).compareTo(money.getAmount()),
                "bill amount: " + money.getAmount());
    }

    /**
     * Try billing client who has not left the parking yet. InvalidInputException expected.
     *
     * @throws Exception
     */
    @Test
    public void billKoInvalidInputException() throws Exception {
        Client client = new Client();
        client.setPlate("ABC123");
        client.setParkingId(parking.getId());
        client.setSlotType(ParkingRepositoryTest.SEDAN);
        client.letIn(0);
        client = clientRepository.save(client);
        Assertions.assertNotNull(client, "saved client null");
        Assertions.assertNotNull(client.getId(), "client Id null");

        // Create new parking
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri + client.getId())).andReturn();
        // check created parking
        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), status,
                "expected error 400 BAD_REQUEST from InvalidInputException. json response: " + content);
    }
}