package demo.easy.parking.billing;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

import org.joda.money.Money;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import demo.easy.parking.client.Client;
import demo.easy.parking.client.ClientRepository;
import demo.easy.parking.parking.Parking;
import demo.easy.parking.parking.ParkingRepositoryTest;
import demo.easy.parking.parking.ParkingService;
import demo.easy.parking.shared.exception.InvalidInputException;

/**
 * BillingServiceImpl unit tests. Dependencies mocked
 */
@SpringBootTest
public class BillingServiceImplTest {

    BillingServiceImpl billingService;

    ParkingService parkingService;

    ClientRepository clientRepository;

    Parking parking;

    @BeforeEach
    public void setUp() {
        parking = ParkingRepositoryTest.buildParkingFixedPlusHours();
        parking.setId(22L);
        // mock parking service
        parkingService = Mockito.mock(ParkingService.class);
        Mockito.when(parkingService.findById(Mockito.anyLong())).thenReturn(parking);
        // mock client repo
        clientRepository = Mockito.mock(ClientRepository.class);
        billingService = new BillingServiceImpl(parkingService, clientRepository);
    }

    /**
     * Call OK to bill client who has already left the parking.
     */
    @Test
    public void billOk() {
        // mock DB client already left
        Client client = new Client();
        client.setId(1L);
        client.setParkingId(parking.getId());
        client.setSlotType(ParkingRepositoryTest.SEDAN);
        client.letIn(0);
        client.letOut();
        client.setDeparture(LocalDateTime.now().plusHours(10));

        Mockito.when(clientRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(client));

        final Money bill = billingService.bill(client.getId());
        Assertions.assertNotNull(bill);
        Assertions.assertNotNull(bill.getAmount());
        Assertions.assertEquals(0, BigDecimal.valueOf(5 + (2 * 10)).compareTo(bill.getAmount()),
                "bill amount: " + bill.getAmount());
    }

    /**
     * Call OK to bill parking with policy by Hours
     */
    @Test
    public void billOkByHours() {
        parking = ParkingRepositoryTest.buildParkingFixedPlusHours();
        parking.setPricingPolicy(PricingPolicy.HOURS);
        parking.setId(22L);
        // mock parking service to return parking by hours
        Mockito.when(parkingService.findById(Mockito.anyLong())).thenReturn(parking);

        // mock DB client already left
        Client client = new Client();
        client.setId(1L);
        client.setParkingId(parking.getId());
        client.setSlotType(ParkingRepositoryTest.SEDAN);
        client.letIn(0);
        client.letOut();
        client.setDeparture(LocalDateTime.now().plusHours(10));

        Mockito.when(clientRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(client));

        final Money bill = billingService.bill(client.getId());
        Assertions.assertNotNull(bill);
        Assertions.assertNotNull(bill.getAmount());
        Assertions.assertEquals(0, BigDecimal.valueOf(2 * 10).compareTo(bill.getAmount()),
                "bill amount: " + bill.getAmount());
    }

    /**
     * Call KO to bill client who has already left the parking
     */
    @Test
    public void billKoClientNotLeft() {
        // mock DB client not left
        Client client = new Client();
        client.setId(1L);
        client.setParkingId(parking.getId());
        client.setSlotType(ParkingRepositoryTest.SEDAN);
        client.letIn(0);
        Mockito.when(clientRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(client));
        Assertions.assertThrows(InvalidInputException.class, () -> billingService.bill(client.getId()));
    }

    /**
     * Call KO to bill non-existing client
     */
    @Test
    public void billKoClientNotExists() {
        // mock DB client not left
        Mockito.when(clientRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());
        Assertions.assertThrows(InvalidInputException.class, () -> billingService.bill(9999L));
    }
}