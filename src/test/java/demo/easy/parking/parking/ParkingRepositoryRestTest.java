package demo.easy.parking.parking;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import demo.easy.parking.AbstractWebTest;

/**
 * Test Parking repository with embedded DB
 */
public class ParkingRepositoryRestTest extends AbstractWebTest {

    public static final String TYPE_35KW = "35kw";
    public static final String SEDAN = "sedan";

    @Autowired
    ParkingRepository parkingRepository;

    /**
     * Test saving parking in DB
     *
     * @throws Exception
     */
    @Test
    public void save() throws Exception {
        Parking parkingInput = ParkingRepositoryTest.buildParkingFixedPlusHours();
        String uri = "/parkingBo";

        // Create new parking
        String inputJson = super.mapToJson(parkingInput);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        // check created parking
        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        Assertions.assertEquals(HttpStatus.CREATED.value(), status, "json response: " + content);
    }

    /**
     * Test saving client in DB with non-existing parking Id. InvalidInputException expected
     */
    @Test
    public void saveKoMissingPlate() throws Exception {
        Parking parkingInput = ParkingRepositoryTest.buildParkingFixedPlusHours();
        parkingInput.setName(null);
        String uri = "/parkingBo";

        // Create new parking
        String inputJson = super.mapToJson(parkingInput);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        // check created parking
        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        Assertions.assertEquals(HttpStatus.FORBIDDEN.value(), status, "json response: " + content);
    }

}