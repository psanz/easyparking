package demo.easy.parking.parking;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import demo.easy.parking.AbstractWebTest;
import demo.easy.parking.billing.PricingPolicyVariable;

/**
 * ParkingController Integration tests (chained WS calls down to embedded DB)
 */
public class ParkingControllerIntegrationTest extends AbstractWebTest {

    public static final String TYPE_50KW = "50kw";

    @Autowired
    ParkingRepository parkingRepository;

    @Autowired
    ParkingService parkingService;

    /**
     * Integration test covering normal lifecycle of a parking resource:
     * Creation + Find + Update + Delete + Find (KO)
     *
     * @throws Exception
     */
    @Test
    public void createFindUpdateDeleteFind() throws Exception {
        ParkingDto parkingDtoInput = ParkingDtoMapper.toDto(ParkingRepositoryTest.buildParkingFixedPlusHours());
        String uri = "/parking";

        // Create new parking
        String inputJson = super.mapToJson(parkingDtoInput);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        // check created parking
        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        Assertions.assertEquals(HttpStatus.CREATED.value(), status, "json response: " + content);
        ParkingDto parkingDtoCreated = super.mapFromJson(content, ParkingDto.class);
        Assertions.assertNotNull(parkingDtoCreated, "parking expected. json response: " + content);
        Assertions.assertNotNull(parkingDtoCreated.getId(), "parking id expected. json response: " + content);
        Assertions.assertNotNull(parkingDtoCreated.getPlacesByType(),
                "parking PlacesByType expected. json response: " + content);
        Assertions.assertEquals(parkingDtoInput.getPlacesByType().get(ParkingRepositoryTest.SEDAN),
                parkingDtoCreated.getPlacesByType().get(ParkingRepositoryTest.SEDAN),
                "parking mismatch. json response: " + content);

        // find parking OK
        mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri + "/" + parkingDtoCreated.getId()))
                .andReturn();
        status = mvcResult.getResponse().getStatus();
        content = mvcResult.getResponse().getContentAsString();
        Assertions.assertEquals(HttpStatus.OK.value(), status, "created parking not found in DB. response: " + content);

        // Update parking remove sedan vehicle, including new 50kw, change 30k capacity
        parkingDtoCreated.getPlacesByType().remove(ParkingRepositoryTest.SEDAN);
        parkingDtoCreated.getPlacesByType().put("50kw", 20);
        parkingDtoCreated.getPlacesByType().put(ParkingRepositoryTest.TYPE_35KW, 30);
        inputJson = super.mapToJson(parkingDtoCreated);
        mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();

        status = mvcResult.getResponse().getStatus();
        content = mvcResult.getResponse().getContentAsString();
        Assertions.assertEquals(HttpStatus.CREATED.value(), status, content);
        ParkingDto parkingDtoUpdated = super.mapFromJson(content, ParkingDto.class);
        Assertions.assertNotNull(parkingDtoUpdated, "parking expected. json response: " + content);
        Assertions.assertEquals(parkingDtoCreated.getId(), parkingDtoUpdated.getId(),
                "parking id mismatch. json response: " + content);
        Assertions.assertNotNull(parkingDtoCreated.getPlacesByType(),
                "parking PlacesByType expected. json response: " + content);
        Assertions.assertNull(parkingDtoCreated.getPlacesByType().get(ParkingRepositoryTest.SEDAN),
                "parking sedan places should no longer be present. json response: " + content);
        Assertions.assertEquals(parkingDtoCreated.getPlacesByType().get(TYPE_50KW),
                parkingDtoCreated.getPlacesByType().get(TYPE_50KW),
                "parking new type 50kw places mismatch. json response: " + content);
        Assertions.assertEquals(parkingDtoCreated.getPlacesByType().get(ParkingRepositoryTest.TYPE_35KW),
                parkingDtoCreated.getPlacesByType().get(ParkingRepositoryTest.TYPE_35KW),
                "parking updated type 35kw places mismatch. json response: " + content);

        // delete parking
        mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri + "/" + parkingDtoUpdated.getId()))
                .andReturn();
        status = mvcResult.getResponse().getStatus();
        content = mvcResult.getResponse().getContentAsString();
        Assertions.assertEquals(HttpStatus.OK.value(), status, content);

        // find parking KO
        mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri + "/" + parkingDtoUpdated.getId()))
                .andReturn();
        status = mvcResult.getResponse().getStatus();
        content = mvcResult.getResponse().getContentAsString();
        Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), status,
                "expected http error 400 when deleted parking not found in DB. response: " + content);
    }

    /**
     * Save Parking KO by field constraint in DB, responding 403 FORBIDDEN
     *
     * @throws Exception
     */
    @Test
    public void saveParkingKoDbConstraint() throws Exception {
        ParkingDto parkingDtoInput = ParkingDtoMapper.toDto(ParkingRepositoryTest.buildParkingFixedPlusHours());
        parkingDtoInput.setName(null);
        String uri = "/parking";

        // Create invalid parking
        String inputJson = super.mapToJson(parkingDtoInput);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        // check error 403
        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        Assertions.assertEquals(HttpStatus.FORBIDDEN.value(), status,
                "expected error 403 FORBIDDEN. response: " + content);
    }

    /**
     * Save Parking KO by Input check responding 400 BAD_REQUEST.
     * Saving parking with required pricing policy variable missing
     *
     * @throws Exception
     */
    @Test
    public void saveParkingKoInvalidInput() throws Exception {
        ParkingDto parkingDtoInput = ParkingDtoMapper.toDto(ParkingRepositoryTest.buildParkingFixedPlusHours());
        parkingDtoInput.getPricingPolicyVariables().remove(PricingPolicyVariable.FIXED);
        String uri = "/parking";

        // Create invalid parking
        String inputJson = super.mapToJson(parkingDtoInput);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();
        // check error 403
        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), status,
                "policy variable missing, expected error 400 BAD_REQUEST. response: " + content);
    }
}