package demo.easy.parking.access;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import demo.easy.parking.AbstractWebTest;
import demo.easy.parking.client.Client;
import demo.easy.parking.parking.Parking;
import demo.easy.parking.parking.ParkingRepository;
import demo.easy.parking.parking.ParkingRepositoryTest;

/**
 * AccessController Integration tests (chained WS calls down to embedded DB)
 */
class AccessControllerIntegrationTest extends AbstractWebTest {

    @Autowired
    ParkingRepository parkingRepository;

    @Autowired
    AccessService accessService;

    // init in setUp
    private Parking parking;

    @Override
    @BeforeEach
    public void setUp() throws Exception {
        super.setUp();
        parking = ParkingRepositoryTest.buildParkingFixedPlusHours();
        parking = parkingRepository.save(parking);
    }

    @AfterEach
    public void tearDown() {
        parkingRepository.delete(parking);
    }

    /**
     * Integration test that covers the access of a client into and outside a parking
     *
     * @throws Exception
     */
    @Test
    public void letInAndOut() throws Exception {
        String uriIn = "/access/in";

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uriIn)
                .param("parkingId", parking.getId().toString())
                .param("plate", "123ABC")
                .param("slotType", ParkingRepositoryTest.SEDAN)
        ).andReturn();

        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        Assertions.assertEquals(HttpStatus.OK.value(), status, content);
        Client client = super.mapFromJson(content, Client.class);
        Assertions.assertNotNull(client, "user expected. IN json response: " + content);
        Assertions
                .assertEquals(parking.getId(), client.getParkingId(), "parking mismatch. IN json response: " + content);
        Assertions.assertEquals(ParkingRepositoryTest.SEDAN, client.getSlotType(),
                "SlotType mismatch. IN json response: " + content);
        Assertions.assertNotNull(client.getSlotIndex(), "SlotIndex expected. IN json response: " + content);
        Assertions.assertNotNull(client.getArrival(), "Arrival time expected. IN json response: " + content);

        String uriOut = "/access/out";

        mvcResult = mvc.perform(MockMvcRequestBuilders.post(uriOut)
                .param("parkingId", parking.getId().toString())
                .param("clientId", client.getId().toString()))
                .andReturn();

        status = mvcResult.getResponse().getStatus();
        content = mvcResult.getResponse().getContentAsString();
        Assertions.assertEquals(HttpStatus.OK.value(), status, content);
        client = super.mapFromJson(content, Client.class);
        Assertions.assertNotNull(client, "user expected. OUT json response: " + content);
        Assertions.assertEquals(parking.getId(), client.getParkingId(),
                "parking mismatch. OUT json response: " + content);
        Assertions.assertEquals(ParkingRepositoryTest.SEDAN, client.getSlotType(),
                "SlotType mismatch. OUT json response: " + content);
        Assertions.assertNotNull(client.getSlotIndex(), "SlotIndex expected. OUT json response: " + content);
        Assertions.assertNotNull(client.getArrival(), "Arrival time expected. OUT json response: " + content);
        Assertions.assertNotNull(client.getDeparture(), "Departure time expected. OUT json response: " + content);
        Assertions.assertNull(client.getBillAmount(), "No bill expected. OUT json response: " + content);
        Assertions.assertNull(client.getBillTime(), "No bill expected. OUT json response: " + content);
    }

    /**
     * Integration test letting in vehicles until the parking is full.
     * Then a client leaves, and another ones comes.
     *
     * @throws Exception
     */
    @Test
    public void fullParking() throws Exception {
        String uriIn = "/access/in";

        List<Client> clients = new ArrayList<>();

        for (int i = 0; i < 12; i++) {
            MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uriIn)
                    .param("parkingId", parking.getId().toString())
                    .param("plate", i + "23ABC")
                    .param("slotType", ParkingRepositoryTest.SEDAN)
            ).andReturn();

            int status = mvcResult.getResponse().getStatus();
            String content = mvcResult.getResponse().getContentAsString();
            if (i <= 9) {
                Assertions
                        .assertEquals(HttpStatus.OK.value(), status, "parking should not be full, content:" + content);
                clients.add(super.mapFromJson(content, Client.class));
            } else if (i == 10) {
                Assertions.assertEquals(HttpStatus.REQUEST_TIMEOUT.value(), status,
                        "parking should be full, content:" + content);
                // first client leaves the parking
                String uriOut = "/access/out";
                mvcResult = mvc.perform(MockMvcRequestBuilders.post(uriOut)
                        .param("parkingId", parking.getId().toString())
                        .param("clientId", clients.get(0).getId().toString()))
                        .andReturn();
                status = mvcResult.getResponse().getStatus();
                content = mvcResult.getResponse().getContentAsString();
                Assertions.assertEquals(HttpStatus.OK.value(), status, content);
            } else { // i = 11
                Assertions.assertEquals(HttpStatus.OK.value(), status,
                        "parking should not be full after client left, content:" + content);
            }
        }
    }
}