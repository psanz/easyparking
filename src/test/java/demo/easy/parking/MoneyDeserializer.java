package demo.easy.parking;

import java.io.IOException;

import org.joda.money.Money;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.DoubleNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import demo.easy.parking.billing.BillingServiceImpl;

public class MoneyDeserializer extends JsonDeserializer<Money> {

    @Override
    public Money deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {

        String currencyCode = BillingServiceImpl.EUR.getCode();
        ObjectMapper mapper = (ObjectMapper) jp.getCodec();
        ObjectNode root = mapper.readTree(jp);
        DoubleNode amountNode = (DoubleNode) root.findValue("amount");
        String amount = null;
        if (null != amountNode) {
            amount = amountNode.asText();
        }

        if (!StringUtils.hasText(amount)) {
            throw new IOException("unable to parse json amount=" + amount);
        }
        return Money.parse(currencyCode + " " + amount);
    }
}