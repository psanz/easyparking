package demo.easy.parking.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.stereotype.Component;

import demo.easy.parking.parking.Parking;
import demo.easy.parking.parking.ParkingRepository;
import demo.easy.parking.shared.exception.InvalidInputException;
import demo.easy.parking.shared.sequence.SequenceGeneratorService;

/**
 * Client Model Listener on Mongo events.
 * Used for generated ids and validation.
 */
@Component
public class ClientMongoListener extends AbstractMongoEventListener<Client> {

    private final SequenceGeneratorService sequenceGenerator;

    private final ParkingRepository parkingRepository;

    @Autowired
    public ClientMongoListener(SequenceGeneratorService sequenceGenerator,
            final ParkingRepository parkingRepository) {
        this.sequenceGenerator = sequenceGenerator;
        this.parkingRepository = parkingRepository;
    }

    @Override
    public void onBeforeConvert(BeforeConvertEvent<Client> event) {
        this.validate(event.getSource());
        if (event.getSource().getId() == null) {
            event.getSource().setId(sequenceGenerator.generateSequence(Client.SEQUENCE_NAME));
        }
    }

    /**
     * Check client validity, checking if is associated to valid parking id and slot type
     *
     * @param client
     *         client to validate
     *
     * @throws demo.easy.parking.shared.exception.InvalidInputException
     *         if invalid
     */
    private void validate(final Client client) {
        if ((client.getSlotType() != null) && (client.getParkingId() != null)) {
            // check parking exists
            final Parking parking = parkingRepository.findById(client.getParkingId()).orElseThrow(() ->
                    new InvalidInputException("Invalid Input Parking Id",
                            "Invalid Input ParkingId" + client.getParkingId().toString()));
            // check slot type exists in parking
            if (!parking.getSlotsByType().containsKey(client.getSlotType())) {
                throw new InvalidInputException("Input Slot Type not present in parking",
                        "Input Slot Type not present in parking. SlotType=" + client.getSlotType());
            }
        } // else we rely on  model constraints to raise an exception 
    }
}
