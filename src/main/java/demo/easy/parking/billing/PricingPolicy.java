package demo.easy.parking.billing;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Pricing Policy enum linking formula name and variable names.
 * Each parking should contain a value for each variable in the formula.
 */
public enum PricingPolicy {

    // based on nb hours
    HOURS("HOURS", Collections.singletonList(PricingPolicyVariable.HOUR_PRICE)),
    // based on fixed amount plus nb hours
    FIXED_PLUS_HOURS("FIXED_PLUS_HOURS",
            Arrays.asList(PricingPolicyVariable.FIXED, PricingPolicyVariable.HOUR_PRICE));

    /**
     * PricingPolicy formula name
     */
    private final String value;

    /**
     * Variables used by pricing policy formula
     */
    private final List<PricingPolicyVariable> vars;

    /**
     * Contructor linking policy name and its variable names
     *
     * @param value
     *         policy name
     * @param vars
     *         policy variables
     */
    PricingPolicy(final String value, List<PricingPolicyVariable> vars) {
        this.value = value;
        this.vars = vars;
    }

    /**
     * ClientStatus value for logging
     *
     * @return String:the name of the ClientStatus enum constant
     */
    public String value() {
        return value;
    }

    /**
     * ClientStatus value for logging
     *
     * @return String:the name of the ClientStatus enum constant
     */
    public List<PricingPolicyVariable> getVars() {
        return vars;
    }
}
