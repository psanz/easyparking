package demo.easy.parking.billing;

import org.joda.money.Money;

public interface BillingService {

    /**
     * @param client
     *         client to bill. Should have already left the parking.
     *
     * @return bill money
     *
     * @throws demo.easy.parking.shared.exception.InvalidInputException
     *         if fails to compute bill
     */
    Money bill(Long clientId);
}
