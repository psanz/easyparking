package demo.easy.parking.billing;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;

import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import demo.easy.parking.client.Client;
import demo.easy.parking.client.ClientRepository;
import demo.easy.parking.parking.Parking;
import demo.easy.parking.parking.ParkingService;
import demo.easy.parking.shared.exception.InvalidInputException;

/**
 * Billing service proxy that select the correct billing implementation based on client's parking pricing policy
 */
@Service
public class BillingServiceImpl implements BillingService {

    /**
     * Default currency
     */
    public static final CurrencyUnit EUR = CurrencyUnit.of("EUR");

    private final ParkingService parkingService;
    private final ClientRepository clientRepository;

    @Autowired
    public BillingServiceImpl(
            final ParkingService parkingService,
            final ClientRepository clientRepository) {
        this.parkingService = parkingService;
        this.clientRepository = clientRepository;
    }

    @Transactional
    @Override
    public Money bill(final Long clientId) {
        Money result;
        Client client = clientRepository.findById(clientId).orElseThrow(() ->
                new InvalidInputException("Invalid clientId=" + clientId));

        final Parking parking = parkingService.findById(client.getParkingId());

        switch (parking.getPricingPolicy()) {
            case HOURS:
                result = billByHours(client, parking);
                break;
            case FIXED_PLUS_HOURS:
                result = billByFixedPlusHours(client, parking);
                break;
            default:
                throw new InvalidInputException(
                        "Parking contains unexpected PricingPolicy: " + parking.getPricingPolicy());
        }

        return result;
    }

    /**
     * Bill client by (hours * HOUR_PRICE) based on PricingPolicy.HOURS
     *
     * @param client
     *         client to bill
     * @param parking
     *         parking with pricing policy variables values
     *
     * @return Money to bill
     */
    private Money billByHours(final Client client, final Parking parking) {
        final LocalDateTime arrival = client.getArrival();
        final LocalDateTime departure = client.getDeparture();

        // check arrival / departure
        if (arrival == null || departure == null) {
            final String msg = String
                    .format("Invalid input client %d . Client should left the parking arrived", client.getId());
            throw new InvalidInputException(msg);
        }

        // process bill
        final long hours = Duration.between(arrival, departure).toHours();
        // hours * HOUR_PRICE
        final BigDecimal amount = parking.getPricingPolicyVariables().get(PricingPolicyVariable.HOUR_PRICE)
                .multiply(BigDecimal.valueOf(hours));
        return Money.of(EUR, amount);
    }

    /**
     * Bill client by (hour * HOUR_PRICE) + FIXED based on PricingPolicy.HOURS and PricingPolicyVariable.FIXED
     *
     * @param client
     *         client to bill
     * @param parking
     *         parking with pricing policy variables values
     *
     * @return Money to bill
     */
    private Money billByFixedPlusHours(final Client client, final Parking parking) {
        final Money bill = this.billByHours(client, parking);
        // (hour * HOUR_PRICE) + FIXED
        return bill.plus(parking.getPricingPolicyVariables().get(PricingPolicyVariable.FIXED));
    }
}

