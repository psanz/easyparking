package demo.easy.parking.billing;

public enum PricingPolicyVariable {

    // based on nb hours
    HOUR_PRICE("HOUR_PRICE"),
    // based on fixed amount plus nb hours
    FIXED("FIXED");

    /**
     * ClientStatus value
     */
    private final String value;

    /**
     * Constructor to use value of enum data.
     *
     * @param value
     *         value of the modification type.
     */
    PricingPolicyVariable(final String value) {
        this.value = value;
    }

    /**
     * ClientStatus value for logging
     *
     * @return String:the name of the ClientStatus enum constant
     */
    public String value() {
        return value;
    }

}
