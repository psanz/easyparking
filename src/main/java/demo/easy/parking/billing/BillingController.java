package demo.easy.parking.billing;

import org.joda.money.Money;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Parking billing Controller to manage billing of clients
 */
@RestController
@RequestMapping(value = "bill")
public class BillingController {

    private final BillingService billingService;

    @Autowired
    public BillingController(BillingService billingService) {
        this.billingService = billingService;
    }

    /**
     * Bill a client based on arrival / departure time and parking PricingPolicy.
     *
     * @param clientId
     *         client Id
     *
     * @return Bill Money
     *
     * @throws demo.easy.parking.shared.exception.InvalidInputException
     *         if client not found or has not left the parking yet
     */
    @PostMapping("{clientId}")
    public ResponseEntity<Money> bill(@PathVariable Long clientId) {
        Money result = billingService.bill(clientId);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
