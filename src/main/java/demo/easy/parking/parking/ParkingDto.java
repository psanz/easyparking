package demo.easy.parking.parking;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import demo.easy.parking.billing.PricingPolicy;
import demo.easy.parking.billing.PricingPolicyVariable;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Parking DTO, with the total number of places from which we build the slot inventory.
 */
@Data
@NoArgsConstructor
public class ParkingDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Auto-generated Id. Optional. Used to override existing parkings.
     */
    private Long id;

    /**
     * Parking name. Not null.
     */
    private String name;

    /**
     * Pricing Policy name. Not null.
     */
    private PricingPolicy pricingPolicy;

    /**
     * Variables associated to Pricing Policy. It should contain all variables present in {@link PricingPolicy#getVars()}
     */
    private Map<PricingPolicyVariable, BigDecimal> pricingPolicyVariables = new EnumMap<>(PricingPolicyVariable.class);

    /**
     * Map Slot Type -> Number of places.
     * LinkedHashMap to keep insertion order.
     * Not Empty.
     */
    private Map<String, Integer> placesByType = new HashMap<>();
}
