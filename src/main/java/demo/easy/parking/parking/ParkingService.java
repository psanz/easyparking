package demo.easy.parking.parking;

/**
 * Service around CRUD operations over Parking business objects
 */
public interface ParkingService {

    /**
     * Find parking or throw InvalidInputException
     *
     * @param parkingId
     *         parking Id
     *
     * @return parking if found
     *
     * @throws demo.easy.parking.shared.exception.InvalidInputException
     *         if not found
     */
    Parking findById(final Long parkingId);

    /**
     * Save Parking.
     * Use null Id to create new Parking, or put a value to update an existing one.
     * When update, the slot availability inventory is transfered to the new one.
     *
     * @param parking
     *         parking
     *
     * @return persisted parking
     *
     * @throws RuntimeException
     *         in anything goes wrong (InvalidInputException, ConstraintViolationException, ...)
     */
    Parking save(Parking parking);

    /**
     * Delete parking
     *
     * @param parkingId
     *         id
     *
     * @throws demo.easy.parking.shared.exception.InvalidInputException
     *         if not found
     */
    void delete(Long parkingId);
}
