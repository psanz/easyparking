package demo.easy.parking.parking;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.springframework.stereotype.Component;

/**
 * Factory to provide Locks by Parking Id. So we can edit a parking ensuring MUTEX.
 */
@Component
public class ParkingLockFactory {
    // concurrent map to make sure there's no concurrent modification in putIfAbsent()
    private final ConcurrentHashMap<Long, Lock> locksById = new ConcurrentHashMap<>();

    public Lock getLock(Long parkingId) {
        locksById.putIfAbsent(parkingId, new ReentrantLock());
        return locksById.get(parkingId);
    }
}
