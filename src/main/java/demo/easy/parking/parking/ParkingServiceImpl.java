package demo.easy.parking.parking;

import java.util.concurrent.locks.Lock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.easy.parking.shared.exception.InvalidInputException;

@Service
public class ParkingServiceImpl implements ParkingService {

    private final ParkingRepository parkingRepository;
    private final ParkingLockFactory lockFactory;

    @Autowired
    public ParkingServiceImpl(final ParkingRepository parkingRepository,
            final ParkingLockFactory lockFactory) {
        this.parkingRepository = parkingRepository;
        this.lockFactory = lockFactory;
    }

    @Override
    public Parking findById(final Long parkingId) {
        return parkingRepository.findById(parkingId).orElseThrow(() ->
                new InvalidInputException("Invalid ParkingId=" + parkingId));
    }

    @Override
    public Parking save(final Parking newParking) {
        if (newParking.getId() != null) {
            final Parking oldParking = parkingRepository.findById(newParking.getId())
                    .orElseThrow(() -> new InvalidInputException("Invalid ParkingId=" + newParking.getId()));

            final Lock lock = lockFactory.getLock(oldParking.getId());
            try {
                lock.lock();
                oldParking.getSlotsByType().forEach((type, oldSlots) -> {
                    final boolean[] newSlots = newParking.getSlotsByType().get(type);
                    if (newSlots != null) {
                        // copy old slot state to new slots
                        for (int i = 0; i < newSlots.length && i < oldSlots.length; i++) {
                            newSlots[i] = oldSlots[i];
                        }
                    }
                });
            } finally {
                lock.unlock();
            }
        }
        return parkingRepository.save(newParking);
    }

    @Override
    public void delete(final Long parkingId) {
        parkingRepository.deleteById(parkingId);

    }
}
