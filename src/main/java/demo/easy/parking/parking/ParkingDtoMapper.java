package demo.easy.parking.parking;

public class ParkingDtoMapper {

    /**
     * Static class
     */
    private ParkingDtoMapper() {
    }

    /**
     * DTO to Model. DTO number of places mapped to slot inventory.
     *
     * @param dto
     *         parking DTO
     *
     * @return model
     */
    public static Parking toModel(ParkingDto dto) {
        Parking result = new Parking();
        result.setId(dto.getId());
        result.setName(dto.getName());
        result.setPricingPolicy(dto.getPricingPolicy());
        result.setPricingPolicyVariables(dto.getPricingPolicyVariables());
        dto.getPlacesByType().forEach((k, v) -> result.getSlotsByType().put(k, new boolean[v]));
        return result;
    }

    /**
     * Model to DTO. lot inventory mapped to DTO number of places.
     *
     * @param model
     *         parking model
     *
     * @return DTO
     */
    public static ParkingDto toDto(Parking model) {
        ParkingDto result = new ParkingDto();
        result.setId(model.getId());
        result.setName(model.getName());
        result.setPricingPolicy(model.getPricingPolicy());
        result.setPricingPolicyVariables(model.getPricingPolicyVariables());
        model.getSlotsByType().forEach((k, v) -> result.getPlacesByType().put(k, v.length));
        return result;
    }
}
