package demo.easy.parking.parking;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import demo.easy.parking.billing.PricingPolicy;
import demo.easy.parking.billing.PricingPolicyVariable;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Parking Business Object.
 * Deals with parking slot inventory synchronizing tasks at instance level
 */
@Document
@Data
@NoArgsConstructor
public class Parking implements Serializable {

    /**
     * Nb times we will try to acquire a free slot
     */
    public static final int NB_TRIES = 10;
    /**
     * Wait ms between each try to to acquire a free slot
     */
    public static final int TRY_WAIT_MS = 1000;
    /**
     * Incremental autogenerated sequence name
     */
    public static final String SEQUENCE_NAME = "inventory_sequence";
    /**
     * Error Messages
     */

    public static final String EMPTY_PARKING = "parking id should not be empty";
    public static final String EMPTY_POLICY = "pricingPolicy should not be empty";
    public static final String EMPTY_INVENTORY = "inventory should not be empty";
    private static final long serialVersionUID = 1L;
    /**
     * Autogenerated Id
     */
    @Id
    private Long id;

    /**
     * Parking name
     */
    @NotEmpty
    @Indexed(unique = true)
    private String name;

    /**
     * Pricing Policy name.
     */
    @NotNull(message = EMPTY_POLICY)
    private PricingPolicy pricingPolicy;

    /**
     * Variables associated to Pricing Policy. It should contain all variables present in {@link PricingPolicy#getVars()}
     */
    @NotEmpty
    private Map<PricingPolicyVariable, BigDecimal> pricingPolicyVariables = new EnumMap<>(PricingPolicyVariable.class);

    /**
     * Map Slot Type -> Array of Slots state (true if free, false if occupied)
     */
    @NotEmpty
    @NotEmpty(message = EMPTY_INVENTORY)
    private Map<String, boolean[]> slotsByType = new HashMap<>();
}
