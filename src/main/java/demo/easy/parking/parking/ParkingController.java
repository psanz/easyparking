package demo.easy.parking.parking;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Parking parking Controller to manage vehicle arrival and departure of parking slots
 */
@RestController
@RequestMapping(value = "parking")
public class ParkingController {

    private final ParkingService parkingService;

    @Autowired
    public ParkingController(ParkingService parkingService) {
        this.parkingService = parkingService;
    }

    /**
     * Save ParkingDto as Parking Model (with slot inventory).
     * Use null Id to create new Parking, or put a value to update an existing one.
     *
     * @param parkingDto
     *         parking DTO (optional Id)
     *
     * @return parkingDto
     * parking DTO with Id
     *
     * @throws exception
     *         if something goes wrong (InvalidInputException, ConstraintViolationException, ...)
     */
    @PostMapping
    public ResponseEntity<ParkingDto> save(@RequestBody ParkingDto parkingDto) {
        Parking parking = ParkingDtoMapper.toModel(parkingDto);
        parking = parkingService.save(parking);
        ParkingDto result = ParkingDtoMapper.toDto(parking);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    /**
     * Delete persisted parking from Id
     *
     * @param parkingId
     *         parking id
     *
     * @return OK if succeed
     *
     * @throws demo.easy.parking.shared.exception.InvalidInputException
     *         if not found
     */
    @DeleteMapping("{parkingId}")
    public ResponseEntity<Object> delete(@PathVariable Long parkingId) {
        parkingService.delete(parkingId);
        return ResponseEntity.ok().build();
    }

    /**
     * Delete persisted parking from Id
     *
     * @param parkingId
     *         parking id
     *
     * @return parking DTO if found
     *
     * @throws demo.easy.parking.shared.exception.InvalidInputException
     *         if not found
     */
    @GetMapping("{parkingId}")
    public ResponseEntity<ParkingDto> find(@PathVariable Long parkingId) {
        Parking parking = parkingService.findById(parkingId);
        ParkingDto result = ParkingDtoMapper.toDto(parking);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
