package demo.easy.parking.parking;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Parking Business Object Rest Repository
 */
@RepositoryRestResource(collectionResourceRel = "parkingBo", path = "parkingBo")
public interface ParkingRepository extends MongoRepository<Parking, Long> {
}