package demo.easy.parking.shared.advice;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import demo.easy.parking.shared.exception.InvalidInputException;
import demo.easy.parking.shared.exception.TimeoutException;

/**
 * Controller Advices to handle WS Exceptions
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    /**
     * InvalidInputException Mapper returning http status code 400 (BAD REQUEST)
     * Raised manually during input validation
     */
    @ExceptionHandler(InvalidInputException.class)
    public ResponseEntity<List<String>> invalidInputExceptionHandler(InvalidInputException ex, WebRequest request) {
        return new ResponseEntity<>(ex.getErrors(), HttpStatus.BAD_REQUEST);
    }

    /**
     * ConstraintViolationException handler returning http status code 403 (FORBIDDEN)
     * Raised by Entity javax validation
     */
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<List<String>> constraintViolationExceptionHandler(ConstraintViolationException ex,
            WebRequest request) {
        final List<String> errors = ex.getConstraintViolations().stream().map(violation ->
                violation.getPropertyPath().toString() + ".\n" + violation.getMessage())
                .collect(Collectors.toList());
        return new ResponseEntity<>(errors, HttpStatus.FORBIDDEN);
    }

    /**
     * TimeoutException returning http error code 408 (REQUEST_TIMEOUT).
     * Raised manually when we failed to acquire a free slot for a new client.
     */
    @ExceptionHandler(TimeoutException.class)
    public ResponseEntity<List<String>> timeoutExceptionHandler(TimeoutException ex, WebRequest request) {
        List<String> errors = Collections.singletonList("Parking Full. Please retry later");
        return new ResponseEntity<>(errors, HttpStatus.REQUEST_TIMEOUT);
    }

    /**
     * Generic Exception Mapper returning http error code 56 (INTERNAL_SERVER_ERROR)
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<List<String>> globleExceptionHandler(Exception ex, WebRequest request) {
        List<String> errors = Arrays.asList(
                ex.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(errors, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
