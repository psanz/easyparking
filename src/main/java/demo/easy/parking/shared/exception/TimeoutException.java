package demo.easy.parking.shared.exception;

/**
 * Timeout RuntimeException.
 * Raised manually during input validation.
 * Extending RuntimeException because it might not be handled by the main stack.
 */
public class TimeoutException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public TimeoutException(final String message) {
        super(message);
    }
}
