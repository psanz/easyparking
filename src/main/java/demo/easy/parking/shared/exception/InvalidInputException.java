package demo.easy.parking.shared.exception;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Invalid Input RuntimeException.
 * Raised manually during input validation.
 * Extending RuntimeException because it might not be handled by the main stack.
 */
public class InvalidInputException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private final List<String> errors;

    public InvalidInputException(final String message) {
        super(message);
        this.errors = Collections.singletonList(message);
    }

    public InvalidInputException(final String message, final List<String> errors) {
        super(message);
        this.errors = errors;
    }

    public InvalidInputException(String message, String... errors) {
        this(message, Arrays.asList(errors));
    }

    public List<String> getErrors() {
        return this.errors;
    }

    @Override
    public String toString() {
        return "InvalidInputException:" + this.getMessage() + "\n errors=" + errors + "} ";
    }
}
