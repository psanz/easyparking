package demo.easy.parking.access;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.concurrent.locks.Lock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import demo.easy.parking.client.Client;
import demo.easy.parking.client.ClientRepository;
import demo.easy.parking.parking.Parking;
import demo.easy.parking.parking.ParkingLockFactory;
import demo.easy.parking.parking.ParkingRepository;
import demo.easy.parking.parking.ParkingService;
import demo.easy.parking.shared.exception.InvalidInputException;
import demo.easy.parking.shared.exception.TimeoutException;

@Service
public class AccessServiceImpl implements AccessService {

    /**
     * Nb times we will try to acquire a free slot
     */
    private static final int NB_TRIES = 10;
    /**
     * Wait milliseconds between each try to to acquire a free slot
     */
    private static final int TRY_WAIT_MS = 1000;
    private final Logger logger = LoggerFactory.getLogger(AccessServiceImpl.class);
    private final ParkingService parkingService;
    private final ClientRepository clientRepository;
    private final ParkingLockFactory parkingLockFactory;
    private final ParkingRepository parkingRepository;

    @Autowired
    public AccessServiceImpl(final ParkingService parkingService,
            final ClientRepository clientRepository,
            final ParkingLockFactory parkingLockFactory,
            final ParkingRepository parkingRepository) {
        this.parkingService = parkingService;
        this.clientRepository = clientRepository;
        this.parkingLockFactory = parkingLockFactory;
        this.parkingRepository = parkingRepository;
    }

    @Transactional
    @Override
    public Client letIn(final Long parkingId, final String plate, final String slotType) {
        Client client = new Client();
        client.setParkingId(parkingId);
        client.setPlate(plate);
        client.setSlotType(slotType);
        client = clientRepository.save(client); // here we also validate the required client fields

        tryAcquireSlot(client);

        client.setArrival(LocalDateTime.now());
        client = clientRepository.save(client); // update arrival time
        return client;
    }

    @Transactional
    @Override
    public Client letOut(Long parkingId, Long clientId) {
        Client client = clientRepository.findById(clientId)
                .orElseThrow(() -> new InvalidInputException("Invalid client Id" + clientId));
        if (!Objects.equals(client.getParkingId(), parkingId)) {
            throw new InvalidInputException("Client does not belong to the Parking",
                    "Client does not belong to the Parking. clientId= " + clientId + ". parkingId= " + parkingId);
        }
        releaseSlot(client);
        client = clientRepository.save(client);
        return client;
    }

    /**
     * Try acquire parking slot and include it into {@link Client#letIn(Integer)}.
     * Parking inventory is updated and Client info is stored in DB.
     * May raise TimeoutException if no place is free after.
     *
     * @param client
     *         client
     *
     * @return client info, with slot type and slot index
     *
     * @throws demo.easy.parking.shared.exception.TimeoutException
     *         if fails to acquire free place
     */
    private void tryAcquireSlot(final Client client) {
        validateClientSlotType(client);
        final Lock lock = parkingLockFactory.getLock(client.getParkingId());
        try { // thread-safe operation by parking parking instance
            lock.lock();
            final Parking parking = parkingService.findById(client.getParkingId());
            final boolean[] slots = parking.getSlotsByType().get(client.getSlotType());
            for (int i = 0; i < slots.length; i++) {
                if (!slots[i]) {
                    slots[i] = true;
                    parkingRepository.save(parking);
                    client.letIn(i);
                    logger.info("Client {} parked on {} {} slot.\n", client.getId(), client.getSlotIndex(),
                            client.getSlotType());
                    break;
                }
            }
        } finally {
            lock.unlock();
        }
        if (client.getSlotIndex() == null) {
            throw new TimeoutException(String.format("Failed to acquire slot after %d * %dms", NB_TRIES, TRY_WAIT_MS));
        }
    }

    /**
     * Release parking slot, updating DB
     *
     * @param client
     *         client leaving
     */
    private void releaseSlot(final Client client) {
        validateClientSlotType(client);
        final Lock lock = parkingLockFactory.getLock(client.getParkingId());
        try { // thread-safe operation to release parking slot
            lock.lock();
            final Parking parking = parkingService.findById(client.getParkingId());
            final boolean[] slots = parking.getSlotsByType().get(client.getSlotType());
            // release the parking slot
            parking.getSlotsByType().get(client.getSlotType());
            slots[client.getSlotIndex()] = false;
            parkingRepository.save(parking);
            client.letOut();
            logger.info("Client {} left {} {} slot.\n", client.getId(), client.getSlotIndex(),
                    client.getSlotType());

        } finally {
            lock.unlock();
        }
    }

    /**
     * Check client SlotType is present in Parking
     *
     * @param client
     *         client
     */
    private void validateClientSlotType(final Client client) {
        final Parking parking = parkingService.findById(client.getParkingId());
        if (!parking.getSlotsByType().containsKey(client.getSlotType())) {
            logger.error("Client Slot Type not found in Parking Inventory.\n{}\n{}", client, this);
            throw new InvalidInputException("Client Slot Type not found in Parking Inventory",
                    "Client Slot Type not found in Parking Inventory " + client.toString());
        }
    }
}
