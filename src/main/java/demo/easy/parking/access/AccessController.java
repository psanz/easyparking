package demo.easy.parking.access;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import demo.easy.parking.client.Client;

/**
 * Parking parking Controller to manage vehicle arrival and departure of parking slots
 */
@RestController
@RequestMapping(value = "access")
public class AccessController {

    private final AccessService accessService;

    @Autowired
    public AccessController(AccessService accessService) {
        this.accessService = accessService;
    }

    /**
     * Petition to let new vehicle into the parking, acquiring parking slot and include it into {@link Client#letIn(Integer)}.
     * Parking slot inventory is updated and Client info is stored in DB.
     * May raise TimeoutException if no place is available.
     *
     * @param parkingId
     *         parking Id
     * @param plate
     *         vehicle plate
     * @param slotType
     *         slot type
     *
     * @return client info, with slot type and slot index
     *
     * @throws demo.easy.parking.shared.exception.TimeoutException
     *         if full
     */
    @PostMapping("/in")
    public ResponseEntity<Client> letIn(Long parkingId, String plate, String slotType) {
        final Client client = accessService.letIn(parkingId, plate, slotType);
        return new ResponseEntity<>(client, HttpStatus.OK);
    }

    /**
     * Let client leave the parking. Parking inventory and Client record updated.
     *
     * @param parkingId
     * @param clientId
     *
     * @return client
     * client info with departure time
     */
    @PostMapping("/out")
    public ResponseEntity<Client> letOut(Long parkingId, Long clientId) {
        final Client client = accessService.letOut(parkingId, clientId);
        return new ResponseEntity<>(client, HttpStatus.OK);
    }
}
