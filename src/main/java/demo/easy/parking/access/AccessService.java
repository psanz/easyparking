package demo.easy.parking.access;

import demo.easy.parking.client.Client;

/**
 * Service dealing with Clients access into / outside parking
 */
public interface AccessService {

    /**
     * Petition to let new vehicle into the parking, acquiring parking slot and include it into {@link Client#letIn(Integer)}.
     * Parking inventory is updated and Client info is stored in DB.
     * May raise TimeoutException if no place is available.
     *
     * @param parkingId
     * @param plate
     * @param slotType
     *
     * @return client info, with slot type and slot index
     *
     * @throws demo.easy.parking.shared.exception.TimeoutException
     *         if no place is free
     */
    Client letIn(Long parkingId, String plate, String slotType);

    /**
     * Let vehicle left the parking.
     * Parking slot is release and client departure info updated in DB.
     *
     * @param parkingId
     * @param clientId
     *
     * @return client
     */
    Client letOut(Long parkingId, Long clientId);

}
