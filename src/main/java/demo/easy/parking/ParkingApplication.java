package demo.easy.parking;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import demo.easy.parking.billing.PricingPolicy;
import demo.easy.parking.billing.PricingPolicyVariable;
import demo.easy.parking.parking.Parking;
import demo.easy.parking.parking.ParkingDto;
import demo.easy.parking.parking.ParkingDtoMapper;
import demo.easy.parking.parking.ParkingRepository;

@SpringBootApplication
public class ParkingApplication {
    private final Logger logger = LoggerFactory.getLogger(ParkingApplication.class);

    private final ParkingRepository parkingRepository;

    public ParkingApplication(final ParkingRepository parkingRepository) {
        this.parkingRepository = parkingRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(ParkingApplication.class);
    }

    /**
     * On App boot
     *
     * @param context
     *         context
     *
     * @return CommandLineRunner
     */
    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext context) {
        initParking();
        return args -> logger.debug("Parking initialized");
    }

    /**
     * Init parking embedded DB. for testing purposes
     */
    private void initParking() {
        ParkingDto parkingDto = new ParkingDto();
        parkingDto.setName("EasyParking");
        parkingDto.setPricingPolicy(PricingPolicy.FIXED_PLUS_HOURS);
        parkingDto.getPricingPolicyVariables().put(PricingPolicyVariable.HOUR_PRICE, BigDecimal.valueOf(2));
        parkingDto.getPricingPolicyVariables().put(PricingPolicyVariable.FIXED, BigDecimal.valueOf(3));
        parkingDto.getPlacesByType().put("SEDAN", 10);
        parkingDto.getPlacesByType().put("20kw", 3);
        parkingDto.getPlacesByType().put("50kw", 1);
        Parking parking = ParkingDtoMapper.toModel(parkingDto);
        parkingRepository.save(parking);
    }

}
